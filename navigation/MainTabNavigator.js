import React from 'react'
import { createBottomTabNavigator } from 'react-navigation'
import AboutScreen from '../screens/AboutScreen'
import { Icon } from 'expo'
import ClientsPage from '../screens/ClientsPage'
import DrugsScreen from '../screens/DrugsScreen'
import ContactUs from '../screens/ContactUs'

export default createBottomTabNavigator(
  {
    'المنشئات الطبية': ClientsPage,
    'العلاج المجانى': DrugsScreen,
    'من نحن': AboutScreen,
    'اتصل بنا': ContactUs
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state
        let iconName

        if (routeName === 'المنشئات الطبية') {
          iconName = `ios-home`
        } else if (routeName === 'اتصل بنا') {
          iconName = `ios-paper-plane`
        } else if (routeName === 'من نحن') {
          iconName = `ios-information-circle`
        } else if (routeName === 'العلاج المجانى') {
          iconName = `ios-flask`
        }

        return (
          <Icon.Ionicons
            name={iconName}
            size={26}
            style={{ marginBottom: -3 }}
            color={tintColor}
          />
        )
      }
    }),
    tabBarOptions: {
      activeTintColor: '#359b45',
      inactiveTintColor: 'gray'
    }
  }
)
