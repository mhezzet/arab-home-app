import React, { Component } from 'react'
import { Query, Mutation } from 'react-apollo'
import gql from 'graphql-tag'
import { FlatList, Image, Picker } from 'react-native'
import { debounce } from 'lodash'

import {
  View,
  Text,
  Content,
  Left,
  Right,
  ListItem,
  Container,
  Button,
  H1,
  H3,
  Icon,
  Form,
  H2,
  Item,
  Input,
  Spinner,
  Label,
  Textarea
} from 'native-base'

import Joi from 'joi-browser'

const schema = Joi.object({
  state: Joi.string()
    .required()
    .error(errors => {
      return {
        message: 'ادخال المنطقة واجب'
      }
    }),
  details: Joi.string()
    .max(500)
    .error(errors => {
      return {
        message: 'التفاصيل كثير تأكد ان لا تذيد عن 500 حرف'
      }
    }),
  name: Joi.string()
    .max(100)
    .required()
    .error(errors => {
      errors.forEach(err => {
        switch (err.type) {
          case 'any.required':
            err.message = 'ادخال الاسم واجب'
            break
          case 'string.max':
            err.message = `الاسم طويل تأكد ان لا يزيد عن 100 حرف`
            break
          default:
            break
        }
      })
      return errors
    }),
  quantity: Joi.number()
    .min(1)
    .required()
    .error(errors => {
      errors.forEach(err => {
        switch (err.type) {
          case 'any.required':
            err.message = 'ادخال الكمية واجب'
            break
          case 'number.min':
            err.message = `الكمية يجب ان تكون اكبر من الصفر`
            break
          default:
            break
        }
      })
      return errors
    }),
  phone: Joi.string()
    .max(20)
    .required()
    .error(errors => {
      return {
        message: 'ادخال رقم الهاتب واجب'
      }
    }),
  address: Joi.string()
    .max(150)
    .required()
    .error(errors => {
      errors.forEach(err => {
        switch (err.type) {
          case 'any.required':
            err.message = 'ادخال العنوان واجب'
            break
          case 'string.max':
            err.message = `العنوان طويل يجب ان يكون اقل من 150 حرف`
            break
          default:
            break
        }
      })
      return errors
    })
})

export default class DrugsScreen extends Component {
  state = {
    view: 'list',
    view2: 1,
    currentDrug: {},
    country: 'مصر',
    regex: '',
    state: '',
    form: {},
    formServerError: '',
    formAppError: ''
  }

  onValueChange(value) {
    this.setState({
      country: value
    })
  }

  formHandler(text, key) {
    this.setState(prevState => ({ form: { ...prevState.form, [key]: text } }))
  }

  changeRegex = text => {
    this.setState({ regex: text })
  }

  render() {
    const changing = debounce(term => this.changeRegex(term), 300)

    return (
      <>
        {this.state.view2 === 2 && (
          <Query
            query={drugs}
            variables={{
              ...(this.state.country && { country: this.state.country }),
              ...(this.state.regex && { regex: this.state.regex }),
              ...(this.state.state && { state: this.state.state }),
              offset: 0,
              limit: 30
            }}
            notifyOnNetworkStatusChange={true}
            fetchPolicy="cache-and-network"
          >
            {({ data, loading, error, fetchMore, networkStatus, refetch }) => {
              if (error)
                return (
                  <Text style={{ textAlign: 'center', marginTop: 80 }}>
                    {error.message}
                  </Text>
                )

              if (data && data.drugs)
                return (
                  <Container>
                    {this.state.view === 'list' && (
                      <>
                        <Item style={{ marginRight: 4, marginLeft: 4 }}>
                          <Icon
                            onPress={() =>
                              this.setState({ view2: 1, regex: '', state: '' })
                            }
                            type="AntDesign"
                            name="back"
                          />
                          <Input onChangeText={changing} placeholder="بحث" />
                          <Icon active name="search" />
                        </Item>

                        <View
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-around'
                          }}
                        >
                          <Button
                            onPress={() => this.setState({ view: 'add' })}
                            style={{ margin: 4 }}
                            light
                          >
                            <Text> اضف او اطلب علاج مجانى </Text>
                          </Button>
                          {data && data.drugs && data.drugs['states'] && (
                            <Picker
                              selectedValue={this.state.state}
                              style={{ height: 50, width: 170 }}
                              onValueChange={itemValue =>
                                this.setState({ state: itemValue })
                              }
                            >
                              <Picker.Item label="اختار المنطقة" value="" />
                              {data &&
                                data.drugs &&
                                data.drugs['states'].map(stat => (
                                  <Picker.Item
                                    key={stat}
                                    label={stat}
                                    value={stat}
                                  />
                                ))}
                            </Picker>
                          )}
                        </View>

                        {data.drugs && data.drugs.drugs.length < 1 && (
                          <H1 style={{ textAlign: 'center', marginTop: 80 }}>
                            لا يوجد نتائج
                          </H1>
                        )}

                        <FlatList
                          refreshing={networkStatus === 4}
                          onRefresh={() => refetch()}
                          data={data.drugs && data.drugs.drugs}
                          renderItem={({ item }) => (
                            <ListItem
                              onPress={() => {
                                this.setState({
                                  currentDrug: item,
                                  view: 'drug'
                                })
                              }}
                            >
                              <Left>
                                <Icon name="arrow-back" />
                              </Left>
                              <Right
                                style={{
                                  display: 'flex',
                                  flexDirection: 'column',
                                  flexGrow: 10,
                                  alignItems: 'flex-end'
                                }}
                              >
                                <View style={{ direction: 'rtl' }}>
                                  <Text style={{ fontWeight: 'bold' }}>
                                    {item.name}
                                  </Text>
                                  <Text style={{ color: 'grey' }}>
                                    {item.details}
                                  </Text>
                                </View>
                              </Right>
                            </ListItem>
                          )}
                          keyExtractor={item => item.id}
                          onEndReachedThreshold={0.5}
                          onEndReached={() => {
                            fetchMore({
                              variables: {
                                offset:
                                  data.drugs && data.drugs.drugs.length + 1
                              },
                              updateQuery: (
                                previousResult,
                                { fetchMoreResult }
                              ) => {
                                if (previousResult === undefined) {
                                  console.log('TODO:fix this bug')
                                  return {}
                                }
                                if (
                                  !fetchMoreResult ||
                                  fetchMoreResult.drugs.length === 0
                                ) {
                                  return previousResult
                                }

                                return {
                                  drugs: {
                                    __typename: 'DrugsResolver',
                                    drugs: [
                                      ...previousResult.drugs.drugs,
                                      ...fetchMoreResult.drugs.drugs
                                    ],
                                    states: fetchMoreResult.drugs.states
                                  }
                                }
                              }
                            })
                          }}
                        />
                        {loading && <Spinner />}
                      </>
                    )}
                    {this.state.view === 'drug' && (
                      <Container>
                        <Content style={{ margin: 15 }}>
                          <Button
                            bordered
                            dark
                            onPress={() => this.setState({ view: 'list' })}
                          >
                            <Text>للخلف</Text>
                          </Button>
                          <View style={{ direction: 'rtl' }}>
                            <H1
                              style={{
                                textAlign: 'center',
                                fontWeight: 'bold',
                                marginBottom: 10
                              }}
                            >
                              {this.state.currentDrug.name}
                            </H1>
                            <H3 style={{ textAlign: 'right', marginBottom: 5 }}>
                              {this.state.currentDrug.details}
                            </H3>
                            <Text
                              style={{ fontWeight: 'bold', marginBottom: 5 }}
                            >
                              العنوان
                            </Text>
                            <Text
                              style={{ textAlign: 'right', marginBottom: 10 }}
                            >
                              {this.state.currentDrug.address}
                            </Text>
                            <Text
                              style={{ fontWeight: 'bold', marginBottom: 5 }}
                            >
                              التليفون
                            </Text>
                            <Text
                              style={{ textAlign: 'right', marginBottom: 10 }}
                            >
                              {this.state.currentDrug.phone}
                            </Text>
                            <Text
                              style={{ fontWeight: 'bold', marginBottom: 5 }}
                            >
                              الكمية
                            </Text>
                            <Text
                              style={{ textAlign: 'right', marginBottom: 10 }}
                            >
                              {this.state.currentDrug.quantity}
                            </Text>
                          </View>
                        </Content>
                      </Container>
                    )}
                    {this.state.view === 'add' && (
                      <Container>
                        <Content>
                          <Button
                            style={{ margin: 15 }}
                            bordered
                            dark
                            onPress={() => this.setState({ view: 'list' })}
                          >
                            <Text>للخلف</Text>
                          </Button>
                          {this.state.formServerError !== '' && (
                            <Text style={{ color: 'red', textAlign: 'center' }}>
                              {this.state.formServerError}
                            </Text>
                          )}
                          <Form style={{ margin: 10 }}>
                            <Item
                              error={!!this.state.formAppError.name}
                              floatingLabel
                            >
                              <Label>الاسم</Label>
                              <Input
                                onChangeText={text =>
                                  this.formHandler(text, 'name')
                                }
                              />
                            </Item>
                            {this.state.formAppError.name && (
                              <Text style={{ color: 'red' }}>
                                {this.state.formAppError.name}
                              </Text>
                            )}
                            <Item
                              error={!!this.state.formAppError.quantity}
                              floatingLabel
                            >
                              <Label>الكمية</Label>
                              <Input
                                onChangeText={text =>
                                  this.formHandler(parseInt(text), 'quantity')
                                }
                                keyboardType="numeric"
                              />
                            </Item>
                            {this.state.formAppError.quantity && (
                              <Text style={{ color: 'red' }}>
                                {this.state.formAppError.quantity}
                              </Text>
                            )}
                            <Item
                              error={!!this.state.formAppError.phone}
                              floatingLabel
                            >
                              <Label>التليفون</Label>
                              <Input
                                onChangeText={text =>
                                  this.formHandler(text, 'phone')
                                }
                              />
                            </Item>
                            {this.state.formAppError.phone && (
                              <Text style={{ color: 'red' }}>
                                {this.state.formAppError.phone}
                              </Text>
                            )}
                            <Item
                              error={!!this.state.formAppError.address}
                              floatingLabel
                            >
                              <Label>العناون</Label>
                              <Input
                                onChangeText={text =>
                                  this.formHandler(text, 'address')
                                }
                              />
                            </Item>
                            {this.state.formAppError.address && (
                              <Text style={{ color: 'red' }}>
                                {this.state.formAppError.address}
                              </Text>
                            )}
                            <Item
                              error={!!this.state.formAppError.state}
                              style={{ marginBottom: 10 }}
                              floatingLabel
                            >
                              <Label>المنطقة</Label>
                              <Input
                                onChangeText={text =>
                                  this.formHandler(text, 'state')
                                }
                              />
                            </Item>
                            {this.state.formAppError.state && (
                              <Text style={{ color: 'red', marginBottom: 5 }}>
                                {this.state.formAppError.state}
                              </Text>
                            )}
                            <Textarea
                              onChangeText={text =>
                                this.formHandler(text, 'details')
                              }
                              style={{ marginBottom: 5 }}
                              rowSpan={5}
                              bordered
                              placeholder="التفصيل"
                            />
                            {this.state.formAppError.details && (
                              <Text style={{ color: 'red', marginBottom: 5 }}>
                                {this.state.formAppError.details}
                              </Text>
                            )}
                            <View
                              style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'center'
                              }}
                            >
                              <Mutation mutation={ADD_DRUG}>
                                {(addDrug, { loading, err }) => {
                                  return (
                                    <Button
                                      disabled={loading}
                                      style={{ margin: 15, marginBottom: 300 }}
                                      bordered
                                      dark
                                      onPress={() => {
                                        if (err)
                                          this.setState({
                                            formServerError: error.message
                                          })

                                        const { error } = schema.validate(
                                          this.state.form,
                                          { abortEarly: false }
                                        )

                                        if (error) {
                                          const test = error.details.reduce(
                                            (accumulator, currentValue) => {
                                              return {
                                                ...accumulator,
                                                [currentValue.path[0]]:
                                                  currentValue.message
                                              }
                                            },
                                            {}
                                          )
                                          this.setState({
                                            formAppError: test
                                          })
                                          return
                                        }

                                        addDrug({
                                          variables: {
                                            ...this.state.form,
                                            country: this.state.country
                                          }
                                        }).then(data => {
                                          this.setState({ view: 'list' })
                                        })
                                      }}
                                    >
                                      <Text>اضف</Text>
                                    </Button>
                                  )
                                }}
                              </Mutation>
                            </View>
                          </Form>
                        </Content>
                      </Container>
                    )}
                  </Container>
                )
              if (loading) return <Spinner />
            }}
          </Query>
        )}
        {this.state.view2 === 1 && (
          <Container>
            <Content>
              <Form>
                <H2 style={{ textAlign: 'center', marginTop: 30 }}>
                  اختر الدولة
                </H2>
                <Picker
                  mode="dialog"
                  selectedValue={this.state.country}
                  onValueChange={this.onValueChange.bind(this)}
                  itemStyle={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {counts.map(count => (
                    <Picker.Item key={count} label={count} value={count} />
                  ))}
                </Picker>
                <View
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginTop: 50
                  }}
                >
                  <Button
                    bordered
                    dark
                    onPress={() => this.setState({ view: 'list', view2: 2 })}
                  >
                    <Text>بحث</Text>
                  </Button>
                </View>
              </Form>
            </Content>
          </Container>
        )}
      </>
    )
  }
}

const drugs = gql`
  query drugs(
    $offset: Int!
    $limit: Int!
    $country: String!
    $state: String
    $regex: String
  ) {
    drugs(
      offset: $offset
      limit: $limit
      country: $country
      state: $state
      regex: $regex
    ) {
      drugs {
        id
        name
        quantity
        phone
        address
        country
        state
        details
      }
      states
    }
  }
`

const ADD_DRUG = gql`
  mutation addDrug(
    $name: String!
    $quantity: Int!
    $phone: String!
    $address: String!
    $country: String!
    $state: String!
    $details: String
  ) {
    addDrug(
      name: $name
      quantity: $quantity
      phone: $phone
      address: $address
      country: $country
      state: $state
      details: $details
    ) {
      id
      name
      quantity
      phone
      address
      country
      state
      details
    }
  }
`

const counts = [
  'مصر',
  'الجزائر',
  'السودان',
  'العراق',
  'المغرب',
  'السعودية',
  'اليمن',
  'سوريا',
  'تونس',
  'الصومال',
  'الأردن',
  'الإمارات العربية المتحدة',
  'ليبيا',
  'فلسطين',
  'لبنان',
  'عمان',
  'الكويت',
  'موريتانيا',
  'قطر',
  'البحرين',
  'جيبوتي',
  'جزر القمر'
]
