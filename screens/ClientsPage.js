import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'
import { FlatList, Image, Picker } from 'react-native'
import { debounce } from 'lodash'
import {
  View,
  Text,
  Content,
  Left,
  Right,
  ListItem,
  Container,
  Button,
  H1,
  H3,
  Icon,
  Form,
  H2,
  Item,
  Input,
  Spinner
} from 'native-base'

export default class ClientsPage extends Component {
  state = {
    view: 'list',
    view2: 1,
    currentClient: {},
    type: '',
    country: 'مصر',
    regex: '',
    state: ''
  }

  onValueChange(value) {
    this.setState({
      country: value
    })
  }

  changeRegex = text => {
    this.setState({ regex: text })
  }

  render() {
    const changing = debounce(term => this.changeRegex(term), 300)
    return (
      <>
        {this.state.view2 === 2 && (
          <Query
            query={clients}
            variables={{
              ...(this.state.type && { type: this.state.type }),
              ...(this.state.country && { country: this.state.country }),
              ...(this.state.regex && { regex: this.state.regex }),
              ...(this.state.state && { state: this.state.state }),
              offset: 0,
              limit: 40
            }}
            fetchPolicy="cache-and-network"
            notifyOnNetworkStatusChange={true}
          >
            {({ data, loading, error, fetchMore, networkStatus, refetch }) => {
              if (error)
                return (
                  <Text style={{ textAlign: 'center', marginTop: 80 }}>
                    {error.message}
                  </Text>
                )
              console.log(networkStatus)
              return (
                <Container>
                  {this.state.view === 'list' && (
                    <>
                      <Item style={{ marginRight: 4, marginLeft: 4 }}>
                        <Icon
                          onPress={() =>
                            this.setState({
                              view2: 1,
                              regex: '',
                              state: '',
                              type: ''
                            })
                          }
                          type="AntDesign"
                          name="back"
                        />
                        <Input onChangeText={changing} placeholder="بحث" />
                        <Icon
                          style={{
                            color:
                              this.state.regex && loading ? '#3CB371' : 'black'
                          }}
                          name="search"
                        />
                      </Item>
                      <View
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          justifyContent: 'space-around'
                        }}
                      >
                        {data.clients && (
                          <Picker
                            selectedValue={this.state.state}
                            style={{ height: 50, width: 150 }}
                            onValueChange={itemValue =>
                              this.setState({ state: itemValue })
                            }
                          >
                            <Picker.Item label="اختار المنطقة" value="" />
                            {data.clients.states.map(stat => (
                              <Picker.Item
                                key={stat}
                                label={stat}
                                value={stat}
                              />
                            ))}
                          </Picker>
                        )}
                        {data.clients && (
                          <Picker
                            selectedValue={this.state.type}
                            style={{ height: 50, width: 150 }}
                            onValueChange={itemValue =>
                              this.setState({ type: itemValue })
                            }
                          >
                            <Picker.Item label="اختار المنشأة" value="" />
                            {types.map(type => (
                              <Picker.Item
                                key={type}
                                label={type}
                                value={type}
                              />
                            ))}
                          </Picker>
                        )}
                      </View>

                      {data.clients && data.clients.clients.length < 1 && (
                        <H1 style={{ textAlign: 'center', marginTop: 80 }}>
                          لا يوجد نتائج
                        </H1>
                      )}

                      <FlatList
                        refreshing={networkStatus === 4}
                        onRefresh={() => refetch()}
                        data={data.clients && data.clients.clients}
                        renderItem={({ item }) => (
                          <ListItem
                            onPress={() => {
                              this.setState({
                                currentClient: item,
                                view: 'client'
                              })
                            }}
                          >
                            <Left>
                              <Icon name="arrow-back" />
                            </Left>
                            <Right
                              style={{
                                display: 'flex',
                                flexDirection: 'column',
                                flexGrow: 10,
                                alignItems: 'flex-end'
                              }}
                            >
                              <View style={{ direction: 'rtl' }}>
                                <Text style={{ fontWeight: 'bold' }}>
                                  {item.name}
                                </Text>
                                <Text style={{ color: '#3CB371' }}>{`الخصم ${
                                  item.discount
                                }%`}</Text>
                                <Text style={{ color: 'grey' }}>
                                  {item.details}
                                </Text>
                              </View>
                            </Right>
                          </ListItem>
                        )}
                        keyExtractor={item => item.id}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => {
                          fetchMore({
                            variables: {
                              offset:
                                data.clients && data.clients.clients.length + 1
                            },
                            updateQuery: (
                              previousResult,
                              { fetchMoreResult }
                            ) => {
                              if (previousResult === undefined) {
                                console.log('TODO:fix this bug')
                                return {}
                              }
                              if (
                                !fetchMoreResult ||
                                fetchMoreResult.clients.length === 0
                              ) {
                                return previousResult
                              }

                              return {
                                clients: {
                                  __typename: 'ClientsResolver',
                                  clients: [
                                    ...previousResult.clients.clients,
                                    ...fetchMoreResult.clients.clients
                                  ],
                                  states: fetchMoreResult.clients.states
                                }
                              }
                            }
                          })
                        }}
                      />
                      {loading && <Spinner />}
                    </>
                  )}
                  {this.state.view === 'client' && (
                    <Container>
                      <Content style={{ margin: 15 }}>
                        <Button
                          bordered
                          dark
                          onPress={() => this.setState({ view: 'list' })}
                        >
                          <Text>للخلف</Text>
                        </Button>
                        <View style={{ direction: 'rtl' }}>
                          <H1
                            style={{
                              textAlign: 'center',
                              fontWeight: 'bold',
                              marginBottom: 10
                            }}
                          >
                            {this.state.currentClient.name}
                          </H1>
                          <H3 style={{ textAlign: 'right', marginBottom: 5 }}>
                            {this.state.currentClient.details}
                          </H3>
                          <Text
                            style={{ color: '#3CB371', marginBottom: 5 }}
                          >{`الخصم ${
                            this.state.currentClient.discount
                          }%`}</Text>
                          <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                            العنوان
                          </Text>
                          <Text style={{ textAlign: 'right' }}>
                            {this.state.currentClient.address}
                          </Text>
                          <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                            التليفون
                          </Text>
                          <Text style={{ textAlign: 'right' }}>
                            {this.state.currentClient.phone}
                          </Text>
                        </View>
                      </Content>
                    </Container>
                  )}
                </Container>
              )
            }}
          </Query>
        )}
        {this.state.view2 === 1 && (
          <Container>
            <Content>
              <Form>
                <H2 style={{ textAlign: 'center', marginTop: 30 }}>
                  اختر الدولة
                </H2>
                <Picker
                  mode="dialog"
                  selectedValue={this.state.country}
                  onValueChange={this.onValueChange.bind(this)}
                  itemStyle={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  {counts.map(count => (
                    <Picker.Item key={count} label={count} value={count} />
                  ))}
                </Picker>
                <View
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row',
                    marginTop: 50
                  }}
                >
                  <Button
                    bordered
                    dark
                    onPress={() => this.setState({ view: 'list', view2: 2 })}
                  >
                    <Text>بحث</Text>
                  </Button>
                </View>
              </Form>
            </Content>
          </Container>
        )}
      </>
    )
  }
}

const clients = gql`
  query clients(
    $type: String
    $offset: Int!
    $limit: Int!
    $country: String!
    $state: String
    $regex: String
  ) {
    clients(
      type: $type
      offset: $offset
      limit: $limit
      country: $country
      state: $state
      regex: $regex
    ) {
      clients {
        id
        name
        discount
        type
        address
        state
        country
        details
        phone
      }
      states
    }
  }
`

const counts = [
  'مصر',
  'الجزائر',
  'السودان',
  'العراق',
  'المغرب',
  'السعودية',
  'اليمن',
  'سوريا',
  'تونس',
  'الصومال',
  'الأردن',
  'الإمارات العربية المتحدة',
  'ليبيا',
  'فلسطين',
  'لبنان',
  'عمان',
  'الكويت',
  'موريتانيا',
  'قطر',
  'البحرين',
  'جيبوتي',
  'جزر القمر'
]

const types = ['صيدلية', 'مركز تحاليل', 'مركز اشعة', 'عيادة', 'مستشفي']
