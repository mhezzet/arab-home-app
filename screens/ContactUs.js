import React, { Component } from 'react'
import { Text, View, Image, Linking } from 'react-native'
import { Content, Container, Icon, H3 } from 'native-base'

export default class ContactUs extends Component {
  render() {
    return (
      <Container>
        <Content style={{ padding: 5 }}>
          <View
            style={{
              display: 'flex',
              justifyContent: 'center',
              flexDirection: 'row',
              marginTop: 10
            }}
          >
            <Image
              style={{ width: 200, height: 150 }}
              source={require('../assets/images/logo.png')}
            />
          </View>
          <H3 style={{ marginBottom: 20, textAlign: 'center',marginTop:30 }}>اتصل بنا</H3>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: 10,
              alignItems: 'center'
            }}
          >
            <Icon style={{ margin: 5, color: '#D44638' }} name="mail" />
            <Text style={{ fontWeight: 'bold', color: '#D44638' }}>Email</Text>
            <View style={{ flexGrow: 2 }} />
            <Text
              style={{
                textAlign: 'center',
                color: '#1E407C',
                textDecorationLine: 'underline'
              }}
              onPress={() => {
                Linking.openURL('mailto:Sameermohamed156@jmail.com')
              }}
            >
              Sameermohamed156@jmail.com
            </Text>
            <View style={{ flexGrow: 2 }} />
          </View>

          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: 10,
              alignItems: 'center'
            }}
          >
            <Icon
              style={{ margin: 5, color: '#3b5998' }}
              name="logo-facebook"
            />
            <Text style={{ fontWeight: 'bold', color: '#3b5998' }}>
              facebook
            </Text>
            <View style={{ flexGrow: 2 }} />
            <Text
              style={{
                color: '#1E407C',
                textAlign: 'center',
                textDecorationLine: 'underline'
              }}
              onPress={() => {
                Linking.openURL(
                  'https://www.facebook.com/%D8%B5%D8%AD%D8%AA%D9%86%D8%A7-%D9%8A%D8%A7-%D8%B9%D8%B1%D8%A8-2417710774940585/'
                )
              }}
            >
              صحتنا-يا-عرب-2417710774940585/
            </Text>
            <View style={{ flexGrow: 2 }} />
          </View>

          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: 10,
              alignItems: 'center'
            }}
          >
            <Icon style={{ margin: 5, color: '#00acee' }} name="logo-twitter" />
            <Text style={{ fontWeight: 'bold', color: '#00acee' }}>
              twitter
            </Text>
            <View style={{ flexGrow: 2 }} />
            <Text
              style={{
                color: '#1E407C',
                textAlign: 'center',
                textDecorationLine: 'underline'
              }}
              onPress={() => {
                Linking.openURL('https://twitter.com/5ejiBqVFitD2a9P')
              }}
            >
              /5ejiBqVFitD2a9P
            </Text>
            <View style={{ flexGrow: 2 }} />
          </View>

          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginBottom: 10,
              alignItems: 'center'
            }}
          >
            <Icon
              style={{ margin: 5, color: '#25D366' }}
              name="logo-whatsapp"
            />
            <Text style={{ fontWeight: 'bold', color: '#25D366' }}>
              whatsApp
            </Text>
            <View style={{ flexGrow: 2 }} />
            <Text
              style={{
                color: '#1E407C',
                textAlign: 'center',
                textDecorationLine: 'underline'
              }}
              onPress={() => {
                Linking.openURL('tel:+00201013624985')
              }}
            >
              00201013624985
            </Text>
            <View style={{ flexGrow: 2 }} />
          </View>
        </Content>
      </Container>
    )
  }
}
