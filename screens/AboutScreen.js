import React, { Component } from 'react'
import { Text, View, Image, Linking } from 'react-native'
import { Container, Content, H2, Icon } from 'native-base'

export default class AboutScreen extends Component {
  render() {
    return (
      <Container>
        <Content style={{ padding: 5 }}>
          <View
            style={{
              display: 'flex',
              justifyContent: 'center',
              flexDirection: 'row',
              marginTop: 10
            }}
          >
            <Image
              style={{ width: 200, height: 150 }}
              source={require('../assets/images/logo.png')}
            />
          </View>
          <Text style={{ marginBottom: 5, textAlign: 'center', marginTop: 30 }}>
            يوفر التطبيق جميع الخدمات الطبية لكل الشعوب الدول العربية
          </Text>
          <Text style={{ marginBottom: 10, textAlign: 'center' }}>
            يقدم العلاج المجانى لكل الشعوب العربية
          </Text>
          <H2 style={{ marginBottom: 5 }}>خدماتنا</H2>
          <Text style={{ marginBottom: 5 }}>يقدم التطبيق الخدمات التالية</Text>
          <Text style={{ marginBottom: 5 }}>
            ▪ تخفيض على الادوية من الصيدليات بنسبة{' '}
            <Text style={{ fontWeight: 'bold' }}>10%</Text> وننشر اسماء و عناوين
            و تليفونات الصيدليات فى كل الدول العربية التى تقدم التخفيض
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ▪ تخفيض بنسبة <Text style={{ fontWeight: 'bold' }}>25%</Text> على
            الاشعة و التحاليل الطبية و ننشر اسماء و عنواين و تليفونات مراكز
            الاشعة و معامل التحاليل التى تلتزم بالتخفيض فى جميع الدول العربية
          </Text>
          <Text style={{ marginBottom: 10 }}>
            ▪ تخفيض على عيادات الاطباء و المستشفيات و العمليات بنسبة{' '}
            <Text style={{ fontWeight: 'bold' }}>25%</Text> فى جميع العيادات و
            المستشفيات فى الوطن العربى
          </Text>

          <H2 style={{ marginBottom: 5 }}>اعلن عن منشأتك الطبية</H2>
          <Text style={{ marginBottom: 5 }}>
            لنشر اعلانك على التطبيق سواء مستشفى او عيادة او صيدلية او مركز اشعة
            او معمل تحاليل
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ▪ الالتزام بالتخفيض الذى تعلن عنه لمستخدمين التطبيق
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ▪ تدفع مبلغ سنوى كى ينشر الاعلان على التطبيق لمدة عام من تاريخ النشر
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ▫ سعر الاعلان للمنشأت الطبية داخل مصر لمدة عام{' '}
            <Text style={{ fontWeight: 'bold' }}> 375 جنية مصرى</Text> بواقع
            الاعلان 1 جنية فى اليوم
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ▫ سعر الاعلان السنوى للمنشأت الطبية فى جميع الدول العربية{' '}
            <Text style={{ fontWeight: 'bold' }}>25 دولار امريكى</Text> لمدة سنة
            كاملة بموجب 2 دولار فى الشهر
          </Text>
          <Text style={{ marginBottom: 5, fontWeight: 'bold' }}>
            يتم التواصل مع العملاء من خلال التليفونات
          </Text>
          <Text style={{ marginBottom: 5 }}>
            داخل مصر: <Text style={{ fontWeight: 'bold' }}> 01013624985</Text>{' '}
            (واتس اب)
          </Text>
          <Text style={{ marginBottom: 10 }}>
            خارج مصر: <Text style={{ fontWeight: 'bold' }}>00201013624985</Text>{' '}
            (واتس اب)
          </Text>
          <H2 style={{ marginBottom: 5 }}>العلاج المجانى</H2>
          <Text style={{ marginBottom: 5 }}>
            عن ابى هريرة رضى الله عنه ان النبى صلى الله عليه و سلم قال
          </Text>
          <Text style={{ marginBottom: 5 }}>
            من فرج عن اخيه المؤمن كربة من كرب الدنيا فرج الله علية كربة من كرب
            يوم القيامة
          </Text>
          <Text style={{ marginBottom: 5 }}>
            و من ستر مسلم ستر الله عليه فى الدنيا و الاخرة
          </Text>
          <Text style={{ marginBottom: 5 }}>
            و الله تعالى فى عون العبد ما كان العبد فى عون اخيه
          </Text>
          <Text style={{ marginBottom: 5 }}>
            اذا كنت تملك ادوية لا تحتاجها فاعلم ان هناك مريض فى امس الحاجة لهذا
            العلاج و لايملك ثمن شراءه
          </Text>
          <Text style={{ marginBottom: 5 }}>
            اعرض الادوية الموجودة عندك ربما يحتاجها مريض فى بلدك واذا كنت تحتاج
            الى دواء فاعرض اسم الدواء الذى تحتاجه
          </Text>
          <Text style={{ marginBottom: 10 }}>
            بعد التبرع او الحصول على العلاج المجاني ابعت رسالة واتس اب اتصل بنا{' '}
            <Text
              style={{
                color: '#1E407C',
                textDecorationLine: 'underline'
              }}
              onPress={() => {
                Linking.openURL('tel:+00201013624985')
              }}
            >
              01013624985
            </Text>
            📞 واتس لمسح العلاج من التطبيق ونشكرك على فعل الخير والتواصل معنا
          </Text>
          <H2 style={{ marginBottom: 5 }}>عروضنا</H2>
          <Text style={{ marginBottom: 5 }}>
            اينما كنت خارج مصر و فى اى دولة من الدول العربية نقدم لك العلاج
            المصرى ويصلك الى منزلك
          </Text>
          <Text style={{ marginBottom: 5 }}>
            ارسل لنا على{' '}
            <Text style={{ fontWeight: 'bold' }}>00201013624985</Text> (واتس اب)
            رسالة بها
          </Text>
          <Text>▪ اسم الدواء</Text>
          <Text>▪ عنوانك</Text>
          <Text style={{ marginBottom: 5 }}>
            سنرسل لك رسالة على الواتس أب بها التفاصيل سعر الدواء + مصاريف الشحن
          </Text>
        </Content>
      </Container>
    )
  }
}
